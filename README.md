# libsectigo

Nim Library implementing the CNAME method for Domain Control Validation, using Sectigo's Cert-Manager Rest API 


# Sectigo Rest API reference

[Sectigo Rest API refecence](https://sectigo.com/uploads/audio/Certificate-Manager-20.1-Rest-API.html)


## Disclamer, misc

This is implementation is limited to the DCV method, feel free to submit PR, fork or whatever suits you to enhance it.

<small><div>Icons from <a href="https://www.flaticon.com/fr/auteurs/srip" title="srip">srip</a> from <a href="https://www.flaticon.com/fr/" title="Flaticon">www.flaticon.com</a></div></small>