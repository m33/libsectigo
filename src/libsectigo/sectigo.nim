import json
import httpclient
import strutils

# Sectigo WSS API entry point (complete URL)
type
  CM_WSS* {.pure.} = enum
    List = "https://cert-manager.com/api/dcv/v1/validation?&size=200&position=0&org=&department=&domain=&expiresIn=365&org=&department=",
    Get_Validation_Status = "https://cert-manager.com/api/dcv/v2/validation/status",
    Check_Domain_Validation_Status = "https://cert-manager.com/api/dcv/v1/validation?&size=200&position=0&org=&department=&domain=&expiresIn=365&org=&department=",
    Start_New_Validation = "https://cert-manager.com/api/dcv/v1/validation/start/domain/cname"
    Submit_Validation = "https://cert-manager.com/api/dcv/v1/validation/submit/domain/cname"

# Sectigo Network API handler
type
  Cert_Manager* = object
    client: HttpClient
    accept: string
    content_type: string
    login: string
    password: string
    customer: string
    data: string


#
# unescape_safe(string): string
#
# In many cases we don't know for sure if a string from WSS is escaped or not
# Don't mess with exception in code, let's define a macro for that
#
proc unescape_safe(a_string: string): string =
  var unescaped_string: string
  try:
    unescaped_string = unescape(a_string)
  except:
    unescaped_string = a_string

  return unescaped_string
#endproc


#
# initSectigoNetworkAPI(CM): HTTPClient
#
# Open a connection to the network API for one WSS provider
# Return the client object if initialized correctly, withtout connexion exceptions
#
proc initSectigoNetworkAPI*(CM: Cert_Manager): HttpClient =
  var client: HttpClient
  let client_headers = {"Accept": "application/json", "login": CM.login, "password": CM.password,
        "customerUri": CM.customer, "Content-Type": "application/json;charset=utf-8"}

  try:
    client = newHttpClient()
    client.headers = newHttpHeaders(client_headers)
  except:
    echo "Error: can't initialize connexion to WSS. Detail: ", getCurrentExceptionMsg()
    return client

  return client
#endproc


#
# endSectigoNetworkAPI(CM)
#
# Close network socket
#
proc endSectigoNetworkAPI*(CM: Cert_Manager) =
  close(CM.client)
#endproc


#
# getSectigoDomains(CM): JsonNode
#
# get the domain list from Sectigo
#
proc getSectigoDomains*(CM: Cert_Manager): JsonNode =
  var list: Response
  var jlist: JsonNode

  try:
    list = CM.client.request($CM_WSS.List)
  except:
    echo "Error: can't connect to WSS. Detail: ", getCurrentExceptionMsg()
    return jlist

  try:
    jlist = parseJson(list.body)
  except:
    echo "Error: can't parse WSS response to json. Detail: ", getCurrentExceptionMsg()
    return jlist

  return jlist
#endproc


#
# getValidationStatus(CM, domain): JsonNode
#
# get the DCV current status for domain, return the WS response
#
proc getValidationStatus*(CM: Cert_Manager, domain: string): JsonNode =
  var response: Response
  var jresponse: JsonNode

  try:
    response = post(CM.client, $CM_WSS.Get_Validation_Status, "{\"domain\":\"" & domain & "\"}", nil)
  except:
    echo "Error: can't connect to WSS. Detail: ", getCurrentExceptionMsg()
    return jresponse

  jresponse = parseJson(response.body)

  return jresponse
#endproc


#
# startNewValidation(CM, domain): string[]
#
# Start a CNAME DCV for domain, returns the cname and value
#
proc startNewValidation*(CM: Cert_Manager, domain: string): seq[string] =
  var response: Response
  var jresult: JsonNode
  var dcv_entries: seq[string]

  try:
    response = post(CM.client, $CM_WSS.Start_New_Validation, "{\"domain\":\"" & domain & "\"}", nil)
  except:
    echo "Error: can't connect to WSS. Detail: ", getCurrentExceptionMsg()
    return dcv_entries

  try:
    jresult = parseJson(response.body)
  except:
    echo "Error: can't parse WSS response to json. Detail: ", getCurrentExceptionMsg()
    return dcv_entries

  dcv_entries.add(unescape_safe($jresult["host"]))
  dcv_entries.add(unescape_safe($jresult["point"]))
  return dcv_entries
#endproc


#
# submitValidation(CM, domain): bool
#
# Submit a CNAME DCV for domain, hope for the best
# Return true for a success
#
proc submitValidation*(CM: Cert_Manager, domain: string): bool =
  var response: Response
  var jresult: JsonNode
  var dcv_entries: seq[string]

  try:
    response = post(CM.client, $CM_WSS.Submit_Validation, "{\"domain\":\"" & domain & "\"}", nil)
  except:
    echo "Error: can't connect to WSS. Detail: ", getCurrentExceptionMsg()
    return false

  try:
    jresult = parseJson(response.body)
  except:
    echo "Error: can't parse WSS response to json. Detail: ", getCurrentExceptionMsg()
    return false

  return true
#endproc
