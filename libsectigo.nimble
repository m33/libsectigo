# Package

version       = "0.1.0"
author        = "m33"
description   = "A Library implementing the CNAME method for DCV, using Sectigo's Cert-Manager Rest API"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.4.0"
